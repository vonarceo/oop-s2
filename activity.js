console.log("Hello World!")

// Von Arceo B228

// What is the term given to unorganized code that's very hard to work with?

// Answer: Spaghetti Codes

// How are object literals written in JS?

	//Answer: {}

// What do you call the concept of organizing information and functionality to belong to an object?

	// Encapsulation

// If the studentOne object has a method named enroll(), how would you invoke it?

	// Answer: studentOne.enroll()

// True or False: Objects can have objects as properties.

	// Answer: True

// What is the syntax in creating key-value pairs?

	/*let students = {
		name: "Von",
		email: "von@email.com"
	}*/

// True or False: A method can have no parameters and still work.

	// Answer: True

// True or False: Arrays can have objects as elements.

	// Answer: True

// True or False: Arrays are objects.

	// Answer: True

// True or False: Objects can have arrays as properties.

	// Answer: True



let studentTwo = {
	name: "Jane",
	email: "jane@email.com",
	grades: [87, 89, 91, 93],

	avgGrade(){
	    let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
	    
	    console.log(avg)
	},

	willPass(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 85){
			return true
		}
		else {
			return false
		}
	},

	willPassWithHonor(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 90){
			return true
		}
		if(avg >= 85 && avg <= 90) {
			return false
		}
		if(avg < 85){
			return "failed"
		}	
	}

}

let studentThree = {
	name: "Joe",
	email: "joe@email.com",
	grades: [78, 82, 79, 85],

	avgGrade(){
	    let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
	    
	    console.log(avg)
	},


	willPass(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 85){
			return true
		}
		else {
			return false
		}
	},

	willPassWithHonor(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 90){
			return true
		}
		if(avg >= 85 && avg <= 90) {
			return false
		}
		if(avg < 85){
			return "failed"
		}	
	}

}

let studentFour = {
	name: "Jessie",
	email: "jessie@email.com",
	grades: [91, 89, 92, 93],

	avgGrade(){
	    let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
	    
	    console.log(avg)
	},

	willPass(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 85){
			return true
		}
		else {
			return false
		}
	},

	willPassWithHonor(){
		let sum = this.grades.reduce((a, b) => a + b)
	    let avg = sum / this.grades.length
		
		if(avg >= 90){
			return true
		}
		if(avg >= 85 && avg <= 90) {
			return false
		}
		if(avg < 85){
			return "failed"
		}	
	}	

}

let classOf1A = {
		students: [
			{
				name: "John",
				email: "john@mail.com",
				grades: [89, 84, 78, 88]
			},
			{
				name: "Joe",
				email: "joe@mail.com",
				grades: [78, 82, 79, 85]
			},
			{
				name: "Jane",
				email: "jane@mail.com",
				grades: [87, 89, 91, 93]
			},
			{
				name: "Jessie",
				email: "jessie@mail.com",
				grades: [91, 89, 92, 93]
			}
		],

		countHonorStudents(){
			let honorStudents = []

			this.students.forEach((student) => {
				let sum = student.grades.reduce((a,b) => a + b)
				let ave = sum / student.grades.length

				if(ave >= 90) honorStudents.push(ave)
			})

			return honorStudents.length
		},

		honorPercentage(){

			let honor = this.countHonorStudents();
			let totalStudents = this.students.length;
		
			return (100 * honor) / totalStudents;

		
		},

		retrieveHonorStudentInfo(){
			let honorStudents = [];

			this.students.forEach((student) => {
				let sum = student.grades.reduce((a,b) => a + b)
				let ave = sum / student.grades.length

				if(ave >= 90) honorStudents.push({
					aveGrade: ave,
					email: student.email
				})
			})

			return honorStudents;

		},

		sortHonorStudentsByGradeDecs(){
			return this.retrieveHonorStudentInfo().sort((a, b) => {


				if(a.aveGrade > b.aveGrade){
					return -1
				}

				if(a.aveGrade < b.aveGrade){
					return 1
				}

				return 0;

			})
		}
		
	}




// 1. Translate the other students from our boilerplate code into their own respective objects.

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

