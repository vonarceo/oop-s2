//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

    //Spaghetti Code - when codes is not organized enough that is becomes hard to work on it

    //create student one
    // let studentOneName = 'John';
    // let studentOneEmail = 'john@mail.com';
    // let studentOneGrades = [89, 84, 78, 88];

    // //create student two
    // let studentTwoName = 'Joe';
    // let studentTwoEmail = 'joe@mail.com';
    // let studentTwoGrades = [78, 82, 79, 85];

    // //create student three
    // let studentThreeName = 'Jane';
    // let studentThreeEmail = 'jane@mail.com';
    // let studentThreeGrades = [87, 89, 91, 93];

    // //create student four
    // let studentFourName = 'Jessie';
    // let studentFourEmail = 'jessie@mail.com';
    // let studentFourGrades = [91, 89, 92, 93];

    // //actions that students may perform will be lumped together
    // function login(email){
    //     console.log(`${email} has logged in`);
    // }

    // function logout(email){
    //     console.log(`${email} has logged out`);
    // }

    // function listGrades(grades){
    //     grades.forEach(grade => {
    //         console.log(grade);
    //     })
    // }

    //This way of organizing employees is not well organized at all.
    //This will become unmanageable when we add more employees or functions
    //To remedy this, we will create objects






// encapsulation - organizes related information(properties) and behavior(methods)

let studentOne = {
    name: 'John',
    email: 'john@email.com',
    grade: [ 89, 84,78,88 ],


// keyword this refeers to the object encapsulating method


login(){
    console.log(`${this.name} successfully Login`)
},

logout(){
    console.log(`${this.name} successfullyLogout`)
},
listGrades(){
    console.log(`Student one's quarterly averages are ${this.grade}`)
}
}

let studentTwo = {
    name: 'Joe',
    email: 'joe@email.com',
    grade: [ 78, 82,79,85 ],


// keyword this refeers to the object encapsulating method


login(){
    console.log(`${this.name} successfully Login`)
},

logout(){
    console.log(`${this.name} successfullyLogout`)
},
listGrades(){
    console.log(`Student two quarterly averages are ${this.grade}`)
},

avgGrade(x){
    let sum = x.reduce((a, b) => a + b)
    let avg = sum / x.length
    console.log(sum)
    console.log(avg)
}
}

